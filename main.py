


import os

from src.ReadDaten import ReadDaten
from src.SauberDaten import SauberDaten
from src.ModelPred import ModelPred

if __name__ == "__main__":

	# import data
	filein = "nrl.csv"
	loc = os.getcwd() + "/data/"
	rowcnt = None
	delim = ","
	readDaten = ReadDaten(filein, loc, rowcnt, delim)
	readDaten.getData()

	# prepare data
	sauberOut = SauberDaten(readDaten.getDatout())
	sauberOut.cleanData()

	# modelling
	# - modelling variables
	#indepvars = ["overTimeVar","oppTeam","playoffVar","originGames","hardHomeStadiums"]
	indepvars = ["overTimeVar","playoffVar","originGames"
		,"prevWin","oppTeam1","oppTeam2","oppTeam3","oppTeam4","oppTeam5","oppTeam6"
		,"oppTeam7","oppTeam8","oppTeam9","oppTeam10","oppTeam11","oppTeam12"
		,"oppTeam13","oppTeam14","oppTeam15","oppTeam16","oppTeam17","oppTeam18"
		,"oppTeam19","oppTeam20","oppTeam21","homeTeam1","homeTeam2","homeTeam3"
		,"homeTeam4","homeTeam5","homeTeam6","homeTeam7","homeTeam8","homeTeam9"
		,"homeTeam10","homeTeam11","homeTeam12","homeTeam13","homeTeam14"
		,"homeTeam15","homeTeam16","homeTeam17","homeTeam18","homeTeam19"
		,"homeTeam20","homeTeam21","homeTeamRankHigher"
	]
	depvars =  ["homeWinDep"]
	# create modelling object
	modelPred = ModelPred(sauberOut.getDataClean()
		, indepvars
		, depvars
	)
	# create training dataset
	modelPred.trainTestSets()
	# calculate non-pred win percent
	modelPred.homeWinPercent()
	# calculate with non win percent
	modelPred.randomForest()



