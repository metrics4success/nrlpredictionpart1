
import numpy as np
import pandas as pd
from datetime import datetime

class SauberDaten:

	def __init__(self, datain):
		self.datain = datain
		self.varNames = None
		self.dataclean = None

	def cleanData(self):

		print "cleaning data..."
		# 1. get column labels
		self.varNames = self.datain.columns
		# 2. get copy of data
		self.dataclean = self.datain
		# 3. dependent variable: home team win
		self.depVariableMake()
		# 4. independent variablen:
		#	playoff game
		self.playoffGame()
		#	team played as categorical variable
		self.opponentTeam()
		#	home team as categorical variable
		self.homeTeam()
		#	over time - did the game go overtime
		self.overTime()
		#	origin season
		self.originSeason()
		#	last game won
		self.lastHomeGameWon()
		#	create training variable
		self.trainingVariable()
		#	historical home stadium record
		self.homeStadiumRecords()
		#	which team is ahead on the ladder
		self.homeTeamAheadOnLadder()

	def depVariableMake(self):
		# if 4 - Home Score > 5 - Away Score
		# define a win or loss based on the score to a category variable
		self.dataclean['homeWinDep'] = \
		np.where(self.dataclean[self.varNames[4]] >= self.dataclean[self.varNames[5]]
			, 1, 0)

	def playoffGame(self):
		# if 6 - Play Off Game?
		# define playoff game to a binary variable
		self.dataclean['playoffVar'] = \
		np.where(self.dataclean[self.varNames[6]] == 'Y', 1, 0)

	def opponentTeam(self):
		# 3 - Away Team
		# create team lookup dict
		teamNames = self.datain[self.varNames[3]].unique()
		teamDic = {}
		cnt = 1
		for team in teamNames:
			teamDic[team] = cnt
			cnt += 1
		# create categorical team variable from lookup dic
		self.dataclean['oppTeam'] = \
		self.dataclean[self.varNames[3]].map(teamDic)
		# team as dummy
		for vals in self.dataclean['oppTeam'].unique():
			colval = "oppTeam" + str(vals)
			self.dataclean[colval] = np.where(
				self.dataclean['oppTeam'] == vals
				, 1
				, 0
			)

	def homeTeam(self):
		# 2 - home Team
		# create team lookup dict
		teamNames = self.datain[self.varNames[2]].unique()
		teamDic = {}
		cnt = 1
		for team in teamNames:
			teamDic[team] = cnt
			cnt += 1
		# create categorical team variable from lookup dic
		self.dataclean['homeTeam'] = \
		self.dataclean[self.varNames[2]].map(teamDic)
		# team as dummy
		for vals in self.dataclean['homeTeam'].unique():
			colval = "homeTeam" + str(vals)
			self.dataclean[colval] = np.where(
				self.dataclean['homeTeam'] == vals
				, 1
				, 0
			)

	def overTime(self):
		# 7 - Over Time?
		# define if game goes over time to a binary variable
		self.dataclean['overTimeVar'] = \
		np.where(self.dataclean[self.varNames[7]] == 'Y', 1, 0)

	def originSeason(self):
		# convert Date to dateformat
		self.dataclean['Date'] = \
		pd.to_datetime(self.dataclean['Date'], format="%d-%b-%y")
		# create year
		self.dataclean['year'] = self.dataclean['Date'].dt.year
		self.dataclean['month'] = self.dataclean['Date'].dt.month
		# dates: 28.5.14, 18.6.14, 9.7.14, 27.5.15, 17.6.15, 8.7.15
		self.dataclean['originGames'] = np.where(
			self.dataclean['month'] == 6
			, 1
			, 0
		)

	def lastHomeGameWon(self):
		# subset and sort
		self.dataclean = self.dataclean.sort_values(
			['homeTeam','Date']
			, ascending=[True, True]
		)
		# get previous win
		self.dataclean['prevWin'] = self.dataclean['homeWinDep'].shift(1)
		# deal with NaN
		self.dataclean['prevWin'] = np.where(
			pd.isnull(self.dataclean['prevWin'])
			, 0
			, self.dataclean['prevWin']
		)
		# deal with homeTeam not the same
		self.dataclean['prevWin'] = np.where(
			self.dataclean['homeTeam'] == self.dataclean['homeTeam'].shift(1)
			, self.dataclean['prevWin']
			, 0
		)

	def homeStadiumRecords(self):
		# home wins 2013
		homeWins13 = self.dataclean[(self.dataclean['homeWinDep'] == 1) & self.dataclean['year'] == 2013]
		homeWins13Tot = homeWins13[['homeTeam','homeWinDep']].groupby(['homeTeam']).count().sort_values('homeWinDep')
		top8Wins13 = homeWins13Tot.tail(4)
		# home wins 2014
		homeWins14 = self.dataclean[(self.dataclean['homeWinDep'] == 1) & self.dataclean['year'] == 2014]
		homeWins14Tot = homeWins14[['homeTeam','homeWinDep']].groupby(['homeTeam']).count().sort_values('homeWinDep')
		top8Wins14 = homeWins14Tot.tail(4)
		# create variables
		self.dataclean['hardHomeStadiums'] = np.where(
			self.dataclean['homeTeam'].isin(top8Wins13) & self.dataclean['year'] == 2014
			, 1
			, 0
		)
		self.dataclean['hardHomeStadiums'] = np.where(
			self.dataclean['homeTeam'].isin(top8Wins14) & self.dataclean['year'] == 2015
			, 1
			, self.dataclean['hardHomeStadiums']
		)

	def homeTeamAheadOnLadder(self):
		# sort by ascending date
		self.dataclean = self.dataclean.sort_values('Date', ascending=True)
		# create default value for column (even rank or lower)
		self.dataclean["homeTeamRankHigher"] = 0
		# create dictionary lookup
		teamNames = self.datain[self.varNames[2]].unique()
		runningScore = {}
		for team in teamNames:
			runningScore[team] = 0
		# inital value season indicator
		yearst = self.dataclean.iloc[0]['Date'].year
		# create running total by season
		for i, row in enumerate(self.dataclean.values):
			# compare running scores of home and away teams
			if runningScore[row[2]] > runningScore[row[3]]:
				self.dataclean.loc[i, "homeTeamRankHigher"] = 1
			# update running total
			if row[49] == 1:
				runningScore[row[2]] += 1
			else:
				runningScore[row[3]] += 1
			# check if same season
			if row[0].year != yearst:
				# reset tally
				for team in teamNames:
					runningScore[team] = 0
				# new season checker
				yearst = row[0].year


	def trainingVariable(self):
		# creating variable for test and train sets
		self.dataclean['is_train'] = \
		np.where(self.dataclean['month'].isin([2,3,4,5]), 1, 0)
		self.dataclean['is_train'] = \
		np.where(self.dataclean['year'].isin([6,7,8,9,10,11]), 2, self.dataclean['is_train'])

	def getDataClean(self):
		# return modelling variables
		return self.dataclean




