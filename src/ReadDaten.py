
import pandas

class ReadDaten:

	def __init__(self, filein, loc, rowcnt, delim):
		self.filein = loc + filein
		self.rowcnt = rowcnt
		self.delim  = delim
		self.datout = None

	def getData(self):
		print "reading raw daten.."
		self.datout = pandas.read_csv(self.filein
			, nrows = self.rowcnt
			, sep = self.delim
		)

	def getDatout(self):
		return self.datout





