
from sklearn.ensemble import RandomForestClassifier
from sklearn.grid_search import GridSearchCV
from sklearn.metrics import classification_report
import pandas as pd 
import numpy as np


class ModelPred:

	def __init__(self, datainput, indepvars, depvars):
		self.datainput = datainput
		self.train = None
		self.tests = None
		self.indepvars = indepvars
		self.depvars = depvars

	def trainTestSets(self):
		print "\n" + "creating training & test datasets..."
		self.train = self.datainput[self.datainput['is_train'] == 1]
		self.tests = self.datainput[self.datainput['is_train'] == 2]

	def homeWinPercent(self):
		homewins = self.train['homeWinDep'].sum()
		games = self.train['homeWinDep'].count()
		winPercent = homewins * 1.00 / games
		print "home win success rate : {:.2%}".format(winPercent) + "\n"

	def randomForest(self):
		# parameter space
		paramSpace = {
			"max_features": ['auto'],
			"n_estimators": [50, 100, 200],
			"criterion": ["gini","entropy"],
			"min_samples_leaf":[1,2,4,6],
		}
		# init model classifier
		clf = RandomForestClassifier(
			n_jobs=10
		)
		grid = GridSearchCV(clf, paramSpace)
		# training model
		print "training model..."
		X1 = np.array(self.train[self.indepvars])
		y1 = self.train[self.depvars].values.ravel()
		grid.fit(X1, y1)
		print grid.best_score_
		# predicting model
		print "predicting results..."
		X2 = np.array(self.tests[self.indepvars])
		y2 = self.tests[self.depvars].values.ravel()
		ypreds = grid.predict(X2)
		#print classification_report(y2, ypreds)
		print "percentage.."
		print np.mean(ypreds == y2)
		print "percentage actual.."
		print np.mean(y2)


